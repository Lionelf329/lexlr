use crate::Terminal;

/// Defines the regular grammar needed to create a lexer.
///
/// There are two attributes, `strings` and `regexps`, each of type `Vec<Terminal>`. `strings`
/// represents literals words in the regular language, while `regexps` contains regular expressions.
/// When there is ambiguity, the generated parser will prefer to interpret something as a literal
/// rather than as a match for a regular expression, and it will also prefer a regular expression
/// earlier in the list to one that is further on.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Vocabulary {
    /// literal words
    pub strings: Vec<Terminal>,
    /// regular expressions
    pub regexps: Vec<Terminal>,
}
