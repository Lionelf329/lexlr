use crate::{MapInner, RuleElement, SymbolRef};
use std::collections::BTreeMap;

/// A nonterminal symbol in the input grammar.
///
/// A nonterminal definition requires three parts - a unique name, a list of generic parameters
/// (which can be empty), and a list of rules. Each rule is composed of a sequence of rule
/// elements, which can refer to terminals or nonterminals in the language definition by name.
///
/// # Generics
///
/// Generic parameters can be used to prevent grammars from including many repetitions of similar
/// patterns. These work the same way as generic type parameters in most programming languages -
/// as the grammar is being interpreted, separate definitions will be created for each way that
/// the generics are filled in. For example, the following are equivalent:
///
/// Version 1:
///
/// `S` &rarr; `A` `B`
///
/// `A` &rarr; `x`
///
/// `A` &rarr; `A` `x`
///
/// `B` &rarr; `y`
///
/// `B` &rarr; `B` `y`
///
/// Version 2:
///
/// `S` &rarr; `G<x>` `G<y>`
///
/// `G<T>` &rarr; `T`
///
/// `G<T>` &rarr; `G<T>` `T`
///
/// It is okay if generic parameters reuse names from the outer scope, it is assumed that a name
/// in a rule refers to the generic parameter with that name if there is one.
///
/// # Examples
///
/// The following definitions are equivalent:
///
/// `A` &rarr; `B` `+` `C`
///
/// `A` &rarr; `D` `/` `E`
///
/// ```rust
/// use lexlr::{GenericNT, RuleElement};
/// GenericNT::<String> {
///     name: String::from("A"),
///     generics: Vec::new(),
///     rules: [
///         (0, vec![RuleElement::new("B"), RuleElement::new("C")]),
///         (1, vec![RuleElement::new("D"), RuleElement::new("E")]),
///     ].into()
/// };
/// ```
///
/// `OneOrMore<T>` &rarr; `T`
///
/// `OneOrMore<T>` &rarr; `OneOrMore<T>` `T`
///
/// ```rust
/// use lexlr::{GenericNT, RuleElement};
/// GenericNT {
///     name: String::from("OneOrMore"),
///     generics: vec![String::from("T")],
///     rules: [
///         (0, vec![RuleElement::new("T")]),
///         (1, vec![
///             RuleElement {
///                 name: String::from("OneOrMore"),
///                 params: vec![RuleElement::new("T")]
///             },
///             RuleElement::new("T")
///         ])
///     ].into()
/// };
/// ```
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct GenericNT<T> {
    /// Name to be used in context free grammar
    pub name: String,
    /// List of generic parameters
    pub generics: Vec<String>,
    /// List of rules for this nonterminal
    pub rules: BTreeMap<usize, Vec<RuleElement<T>>>,
}

impl<T> GenericNT<T> {
    /// Creates a nonterminal definition from a name, list of generics, and list of rules.
    ///
    /// This is just a convenience wrapper to avoid using `String::from(s)` or `s.to_string()`
    /// when constructing instances by hand.
    pub fn new(
        name: &str,
        generics: Vec<&str>,
        rules: BTreeMap<usize, Vec<RuleElement<T>>>,
    ) -> Self {
        assert!(!rules.is_empty());
        GenericNT {
            name: String::from(name),
            generics: generics.into_iter().map(String::from).collect(),
            rules,
        }
    }
}

impl GenericNT<SymbolRef> {
    /// Returns a modified set of rules where generics have been replaced by the provided
    /// parameters.
    pub fn replace_generics(
        &self,
        params: &[RuleElement<SymbolRef>],
    ) -> BTreeMap<usize, Vec<RuleElement<SymbolRef>>> {
        assert_eq!(self.generics.len(), params.len());
        self.rules.map_inner(|x| x.replace_generics(params))
    }
}

impl RuleElement<SymbolRef> {
    /// Replaces the contents of the rule element based on a list of generics.
    ///
    /// `from` is the list of names in the nonterminal definition, and `to` is the list of elements
    /// that the generics are being instantiated with.
    fn replace_generics(&self, params: &[RuleElement<SymbolRef>]) -> RuleElement<SymbolRef> {
        if !self.params.is_empty() {
            RuleElement {
                name: self.name.clone(),
                params: self
                    .params
                    .iter()
                    .map(|x| x.replace_generics(params))
                    .collect(),
            }
        } else if let SymbolRef::Generic(index) = self.name {
            params[index].clone()
        } else {
            self.clone()
        }
    }
}
