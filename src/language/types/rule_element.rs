use crate::{Language, SymbolRef};
use std::fmt::{Display, Formatter, Result, Write};

/// A symbol that can appear in a rule for a nonterminal in the context free grammar.
///
/// Each element has a name and a set of elements to fill in the generic parameters, if any.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct RuleElement<T> {
    /// Same as `name` field in strings, regexps, or symbols.
    pub name: T,
    /// Generic parameters for nonterminals. Leave empty for terminals.
    pub params: Vec<RuleElement<T>>,
}

impl<T: Clone + Eq> RuleElement<T> {
    /// Creates a `RuleElement` with a provided name and an empty list of parameters.
    pub fn new<U: Into<T>>(name: U) -> Self {
        RuleElement {
            name: name.into(),
            params: Vec::new(),
        }
    }
}

impl RuleElement<SymbolRef> {
    /// Returns a string describing the type of the rule element in the generated code.
    pub fn type_str<T>(&self, generics: &[String], language: &Language<T>) -> String {
        let mut result = self.name.type_str(generics, language).to_string();
        if self.params.len() > 0 {
            result.push('<');
            result.push_str(
                &self
                    .params
                    .iter()
                    .map(|e| e.type_str(generics, language))
                    .collect::<Vec<_>>()
                    .join(", "),
            );
            result.push('>');
        }
        result
    }

    /// Returns a string containing all the information about this rule element.
    pub fn describe<T>(&self, generics: &[String], language: &Language<T>) -> String {
        let mut result = self.name.describe(generics, language).to_string();
        if self.params.len() > 0 {
            result.push('<');
            result.push_str(
                &self
                    .params
                    .iter()
                    .map(|e| e.describe(generics, language))
                    .collect::<Vec<_>>()
                    .join(", "),
            );
            result.push('>');
        }
        result
    }
}

impl Display for RuleElement<String> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        if self.name.chars().all(char::is_alphanumeric) && !self.name.is_empty() {
            self.name.fmt(f)
        } else {
            std::fmt::Debug::fmt(&self.name, f)
        }?;
        if self.params.len() > 0 {
            f.write_char('<')?;
            self.params[0].fmt(f)?;
            for element in &self.params[1..] {
                f.write_str(", ")?;
                element.fmt(f)?;
            }
            f.write_char('>')?;
        }
        Ok(())
    }
}
