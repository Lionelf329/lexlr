use crate::{BTreeMapExt, GenericNT, Language, MapOfSet, RuleElement, SymbolRef};
use std::collections::{BTreeMap, BTreeSet, VecDeque};

impl Language<String> {
    /// Returns a list of nonterminal definitions that form an equivalent grammar with no generics.
    ///
    /// The new grammar retains the necessary information to upgrade it back to the original
    /// definitions. It also computes information such as whether each nonterminal can be matched
    /// by empty input, which terminals they can start with, and which terminals they can be
    /// followed by.
    pub fn resolve_refs(&self) -> (Language<SymbolRef>, LinkerResult) {
        let mut errors = LinkerResult::default();
        let dict = errors.get_dict(&self);
        let symbols = errors.resolve_symbols(&self.symbols, &dict, self);
        let language = Language {
            vocabularies: self.vocabularies.clone(),
            symbols,
        };
        (language, errors)
    }
}

/// Things that can go wrong when resolving language references.
#[derive(Clone, Debug, Default)]
pub struct LinkerResult {
    /// A map of symbol names that are defined multiple times to a set of definitions.
    pub duplicates: BTreeMap<String, BTreeSet<SymbolRef>>,
    /// A list of rule elements that don't point to anything.
    pub not_found: Vec<ElementLocation>,
    /// A list of rule elements with the wrong number of generic parameters.
    pub mismatched_generics: Vec<ElementLocation>,
    /// A set of symbol indices that aren't referenced in any rules.
    pub unused_symbols: BTreeSet<usize>,
    /// A map of symbol indices to the symbol indices that they use, including usages in rules
    /// that could not be fully resolved.
    pub graph: Vec<BTreeSet<usize>>,
}

/// A location of a rule element in a language definition.
#[derive(Clone, Debug)]
pub struct ElementLocation {
    /// The index of the nonterminal definition.
    pub symbol: usize,
    /// The index of the rule within the nonterminal.
    pub rule: usize,
    /// The index of the element within the rule.
    pub element: usize,
    /// The sequence of generic parameter indices to get to the element.
    pub path: Vec<usize>,
}

impl LinkerResult {
    pub fn no_errors(&self) -> bool {
        self.duplicates.is_empty()
            && self.not_found.is_empty()
            && self.mismatched_generics.is_empty()
    }

    pub fn reachable_from(&self, symbol: usize) -> BTreeSet<usize> {
        let mut queue = VecDeque::from([symbol]);
        let mut result = BTreeSet::from([symbol]);
        while let Some(next) = queue.pop_front() {
            for &e in &self.graph[next] {
                if result.insert(e) {
                    queue.push_back(e);
                }
            }
        }
        result
    }

    fn get_dict<'a>(&mut self, language: &'a Language<String>) -> BTreeMap<&'a String, SymbolRef> {
        let mut dict = BTreeMap::new();
        for (i, v) in language.vocabularies.iter().enumerate() {
            for (j, s) in v.strings.iter().enumerate() {
                dict.add(&s.name, SymbolRef::String(i, j));
            }
            for (j, r) in v.regexps.iter().enumerate() {
                dict.add(&r.name, SymbolRef::Regexp(i, j));
            }
        }
        for (i, s) in language.symbols.iter().enumerate() {
            dict.add(&s.name, SymbolRef::Symbol(i));
            self.unused_symbols.insert(i);
        }
        let (dict, duplicates) = dict.one_to_one();
        self.duplicates = duplicates.map_keys(String::clone);
        dict
    }

    fn resolve_symbols(
        &mut self,
        symbols: &[GenericNT<String>],
        dict: &BTreeMap<&String, SymbolRef>,
        language: &Language<String>,
    ) -> Vec<GenericNT<SymbolRef>> {
        self.graph = vec![BTreeSet::new(); symbols.len()];
        symbols
            .iter()
            .enumerate()
            .map(|(symbol_index, s)| GenericNT {
                name: s.name.clone(),
                rules: s
                    .rules
                    .iter()
                    .filter_map(|(&rule_index, rule)| {
                        self.resolve_rule(symbol_index, s, rule_index, rule, dict, language)
                    })
                    .collect(),
                generics: s.generics.clone(),
            })
            .collect()
    }

    fn resolve_rule(
        &mut self,
        symbol_index: usize,
        symbol: &GenericNT<String>,
        rule_index: usize,
        rule: &[RuleElement<String>],
        dict: &BTreeMap<&String, SymbolRef>,
        language: &Language<String>,
    ) -> Option<(usize, Vec<RuleElement<SymbolRef>>)> {
        let mut success = true;
        let mut new_rule = Vec::new();
        for (element_index, e) in rule.iter().enumerate() {
            match self.resolve_element(
                e,
                &symbol.generics,
                dict,
                language,
                &mut ElementLocation {
                    symbol: symbol_index,
                    rule: rule_index,
                    element: element_index,
                    path: Vec::new(),
                },
            ) {
                Some(rule_element) => new_rule.push(rule_element),
                None => success = false,
            }
        }
        if success {
            Some((rule_index, new_rule))
        } else {
            None
        }
    }

    fn resolve_element(
        &mut self,
        element: &RuleElement<String>,
        generics: &[String],
        dict: &BTreeMap<&String, SymbolRef>,
        language: &Language<String>,
        path: &mut ElementLocation,
    ) -> Option<RuleElement<SymbolRef>> {
        if let Some(i) = generics.iter().position(|x| x == &element.name) {
            if element.params.is_empty() {
                Some(RuleElement::new(SymbolRef::Generic(i)))
            } else {
                self.mismatched_generics.push(path.clone());
                None
            }
        } else {
            let mut success = true;
            let name = dict.get(&element.name).copied();
            if let Some(name) = name {
                let length = match name {
                    SymbolRef::String(_, _) | SymbolRef::Regexp(_, _) => 0,
                    SymbolRef::Symbol(i) => {
                        self.unused_symbols.remove(&i);
                        self.graph[path.symbol].insert(i);
                        language.symbols[i].generics.len()
                    }
                    SymbolRef::StartSymbol | SymbolRef::Generic(_) => unreachable!(),
                };
                if element.params.len() != length {
                    success = false;
                    self.mismatched_generics.push(path.clone());
                }
            } else {
                success = false;
                if !self.duplicates.contains_key(&element.name) {
                    self.not_found.push(path.clone());
                }
            }
            let mut params = Vec::new();
            for (i, p) in element.params.iter().enumerate() {
                path.path.push(i);
                match self.resolve_element(p, generics, dict, language, path) {
                    Some(rule_element) => params.push(rule_element),
                    None => success = false,
                }
                path.path.pop();
            }
            if success {
                Some(RuleElement {
                    name: name.unwrap(),
                    params,
                })
            } else {
                None
            }
        }
    }
}
