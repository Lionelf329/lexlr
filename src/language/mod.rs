use crate::language::resolve_refs::ElementLocation;
use std::fmt::{Debug, Display, Formatter, Write};
use std::ops::Index;
pub use types::*;

mod resolve_refs;
mod types;

/// Defines the regular and context free grammars needed to create a parser.
///
/// Each item in the `vocabularies` list represents a lexer mode. Simple language will only need one
/// mode, however multiple will be required when one language is embedded in another.
///
/// The context free grammar is defined using a `Vec<GenericNT>`, which is a list of nonterminal
/// definitions. Each nonterminal can reference literals, regular expressions, or other
/// nonterminals by name.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Language<T> {
    /// sets of terminals, one for each lexer mode
    pub vocabularies: Vec<Vocabulary>,
    /// nonterminals
    pub symbols: Vec<GenericNT<T>>,
}

impl<T> Language<T> {
    /// Returns a list of tokens for each lexer mode, with ignored tokens removed.
    pub fn get_mapping(&self) -> Vec<Vec<Token>> {
        let mut mapping = Vec::new();
        for (i, v) in self.vocabularies.iter().enumerate() {
            let mut list = vec![Token::EOF];
            for (j, s) in v.strings.iter().enumerate() {
                if !s.ignored {
                    list.push(Token::String(i, j));
                }
            }
            for (j, r) in v.regexps.iter().enumerate() {
                if !r.ignored {
                    list.push(Token::Regexp(i, j));
                }
            }
            mapping.push(list);
        }
        mapping
    }

    /// Returns the name associated with a given token.
    pub fn label(&self, token: Token) -> &str {
        match token {
            Token::String(i, j) => &self.vocabularies[i].strings[j].name,
            Token::Regexp(i, j) => &self.vocabularies[i].regexps[j].name,
            Token::EOF => "EOF",
        }
    }
}

impl<T> Index<&ElementLocation> for Language<T> {
    type Output = RuleElement<T>;

    fn index(&self, index: &ElementLocation) -> &Self::Output {
        let mut element = &self.symbols[index.symbol].rules[&index.rule][index.element];
        for &p in &index.path {
            element = &element.params[p];
        }
        element
    }
}

impl Display for Language<String> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for v in &self.vocabularies {
            f.write_str("[strings]\n")?;
            for t in &v.strings {
                match (t.name == t.text, t.ignored) {
                    (true, true) => write!(f, "~{:?}\n", t.name),
                    (true, false) => write!(f, "{:?}\n", t.name),
                    (false, true) => write!(f, "{} ~= {:?}\n", &t.name, &t.text),
                    (false, false) => write!(f, "{} = {:?}\n", &t.name, &t.text),
                }?;
            }
            f.write_str("\n[regexps]\n")?;
            for t in &v.regexps {
                if t.ignored {
                    write!(f, "{} ~= {:?}\n", t.name, t.text)?
                } else {
                    write!(f, "{} = {:?}\n", t.name, t.text)?
                }
            }
            f.write_char('\n')?;
        }
        f.write_str("[symbols]\n")?;
        for s in &self.symbols {
            f.write_char('\n')?;
            let mut len = s.name.len();
            f.write_str(&s.name)?;
            if !s.generics.is_empty() {
                f.write_char('<')?;
                f.write_str(&s.generics[0])?;
                len += s.generics[0].len();
                for g in &s.generics[1..] {
                    f.write_str(", ")?;
                    f.write_str(g)?;
                    len += g.len() + 2;
                }
                f.write_char('>')?;
                len += 2;
            }
            f.write_str(" =")?;
            len += 1;
            let mut rules = s.rules.values();
            for e in rules.next().unwrap() {
                f.write_char(' ')?;
                Display::fmt(e, f)?;
            }
            for rule in rules {
                f.write_char('\n')?;
                for _ in 0..len {
                    f.write_char(' ')?;
                }
                f.write_char('|')?;
                for e in rule {
                    f.write_char(' ')?;
                    Display::fmt(e, f)?;
                }
            }
            f.write_str("\n")?;
        }
        Ok(())
    }
}
