use crate::R;

// Char
pub fn regex0_0(_: &mut State, _: R, s: String) -> Char {
    super::parse_char(&s)
}

// GroupChar
pub fn regex1_0(_: &mut State, _: R, s: String) -> GroupChar {
    super::parse_char(&s)
}

// S -> Or
pub fn rule0_0(_: &mut State, or: Or) -> S {
    or
}

// Or -> Concat
pub fn rule1_0(_: &mut State, concat: Concat) -> Or {
    concat
}

// Or -> Or | Concat
pub fn rule1_1(_: &mut State, or: Or, _: R, concat: Concat) -> Or {
    S::or(or, concat)
}

// Concat -> Sequence<Suffix>
pub fn rule2_0(_: &mut State, mut sequence: Sequence<Suffix>) -> Concat {
    match sequence.len() {
        0 => S::Epsilon,
        1 => sequence.pop().unwrap(),
        _ => S::Concat(sequence),
    }
}

// Suffix -> Unit
pub fn rule3_0(_: &mut State, tree: Unit) -> Suffix {
    tree
}

// Suffix -> Unit *
pub fn rule3_1(_: &mut State, tree: Unit, _: R) -> Suffix {
    S::star(tree)
}

// Suffix -> Unit +
pub fn rule3_2(_: &mut State, tree: Unit, _: R) -> Suffix {
    S::plus(tree)
}

// Suffix -> Unit ?
pub fn rule3_3(_: &mut State, tree: Unit, _: R) -> Suffix {
    S::optional(tree)
}

// Unit -> Char
pub fn rule4_0(_: &mut State, char: Char) -> Unit {
    S::char(char)
}

// Unit -> Group
pub fn rule4_1(_: &mut State, group: Group) -> Unit {
    group
}

// Unit -> ( S )
pub fn rule4_2(_: &mut State, _: R, tree: S, _: R) -> Unit {
    tree
}

// Group -> [ GroupContents ]
pub fn rule5_0(_: &mut State, _: R, ranges: GroupContents, _: R) -> Group {
    super::build_group(ranges, false)
}

// Group -> [^ GroupContents ]
pub fn rule5_1(_: &mut State, _: R, ranges: GroupContents, _: R) -> Group {
    super::build_group(ranges, true)
}

// GroupContents -> -
pub fn rule6_0(_: &mut State, _: R) -> GroupContents {
    vec!['-'..='-']
}

// GroupContents -> GroupMember
pub fn rule6_1(_: &mut State, item: GroupMember) -> GroupContents {
    vec![item]
}

// GroupContents -> GroupContents GroupMember
pub fn rule6_2(_: &mut State, mut vec: GroupContents, item: GroupMember) -> GroupContents {
    vec.push(item);
    vec
}

// GroupMember -> GroupChar
pub fn rule7_0(_: &mut State, c1: GroupChar) -> GroupMember {
    c1..=c1
}

// GroupMember -> GroupChar - GroupChar
pub fn rule7_1(_: &mut State, c1: GroupChar, _: R, c2: GroupChar) -> GroupMember {
    c1..=c2
}

// Sequence<T> -> ~epsilon
pub fn rule8_0<T>(_: &mut State) -> Sequence<T> {
    Vec::new()
}

// Sequence<T> -> Sequence<T> T
pub fn rule8_1<T>(_: &mut State, mut vec: Sequence<T>, item: T) -> Sequence<T> {
    vec.push(item);
    vec
}

pub type State = ();
pub type Char = char;
pub type GroupChar = char;
pub type S = super::RegexTree;
pub type Or = super::RegexTree;
pub type Concat = super::RegexTree;
pub type Suffix = super::RegexTree;
pub type Unit = super::RegexTree;
pub type Group = super::RegexTree;
pub type GroupContents = Vec<GroupMember>;
pub type GroupMember = std::ops::RangeInclusive<char>;
pub type Sequence<T> = Vec<T>;
