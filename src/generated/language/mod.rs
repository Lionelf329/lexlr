use crate::Parse;

mod generated;
mod reducers;

impl crate::Language<String> {
    /// Parses a language definition given as a string.
    pub fn new(input: &str) -> Self {
        let result = input
            .chars()
            .filter(|&x| x != '\r')
            .parse(&mut (), &generated::DATA);
        match result {
            Ok(language) => language,
            Err(range) => {
                let error = input[range.range()].to_owned();
                panic!("{}", error);
            }
        }
    }
}
