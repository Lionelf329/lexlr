use super::reducers::*;
use crate::{pop_many, Action, Action::*, StaticParserData, SymbolExt, Token, ALPHABET_SIZE, R};
use Symbol::*;

pub const DATA: StaticParserData<Symbol> = StaticParserData::new(&LEXER, &LABELS, &STATES, goto);

const LEXER: [&[[usize; ALPHABET_SIZE]]; 1] = [&[
    [
        2, 3, 0, 2, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,
        0, 7, 8, 9, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 10, 0, 0, 0, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 0, 11, 0, 12, 0,
    ],
    [
        2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        4, 0, 4, 4, 4, 13, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 14, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 4, 4, 4, 4, 4, 4,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 0,
        0, 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 0, 0, 0, 0, 6, 0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 16, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        4, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
        4, 4, 4, 4, 4, 4, 4, 4, 4,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 18, 0, 0,
        0, 0, 19, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 21, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 26, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 27, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 28, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 33, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 34, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 35, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 36, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 37, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    [
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
]];

const LABELS: [&[Option<(Token, Option<usize>)>]; 1] = [&[
    None,
    Some((Token::Regexp(0, 2), None)),
    Some((Token::String(0, 9), Some(10))),
    None,
    Some((Token::String(0, 8), Some(9))),
    Some((Token::Regexp(0, 0), Some(11))),
    Some((Token::String(0, 5), Some(6))),
    Some((Token::String(0, 3), Some(4))),
    Some((Token::String(0, 6), Some(7))),
    None,
    Some((Token::String(0, 7), Some(8))),
    Some((Token::String(0, 4), Some(5))),
    Some((Token::Regexp(0, 1), Some(12))),
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    None,
    Some((Token::String(0, 1), Some(2))),
    Some((Token::String(0, 0), Some(1))),
    Some((Token::String(0, 2), Some(3))),
]];

const STATES: &[(usize, &[Action])] = &[
    (
        0,
        &[
            Error,
            Reduce(2, 0),
            Error,
            Reduce(2, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Accept, Error, Error, Error, Error, Error, Error, Error, Error, Error, Error, Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Shift(3),
            Error,
            Shift(4),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(6),
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(6),
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Reduce(2, 1),
            Error,
            Reduce(2, 1),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
            Error,
            Reduce(6, 0),
            Error,
            Error,
            Reduce(6, 0),
            Error,
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Reduce(8, 0),
            Error,
            Error,
            Reduce(8, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(8, 0),
            Reduce(8, 0),
        ],
    ),
    (
        0,
        &[
            Reduce(4, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(4, 0),
            Error,
        ],
    ),
    (
        0,
        &[
            Reduce(3, 0),
            Reduce(3, 0),
            Reduce(3, 0),
            Reduce(3, 0),
            Error,
            Reduce(3, 0),
            Error,
            Error,
            Reduce(3, 0),
            Error,
            Shift(12),
            Reduce(3, 0),
            Reduce(3, 0),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Shift(13),
            Error,
            Error,
            Shift(14),
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(15),
            Reduce(16, 0),
        ],
    ),
    (
        0,
        &[
            Reduce(1, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(18),
            Error,
        ],
    ),
    (
        0,
        &[
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
            Error,
            Reduce(6, 1),
            Error,
            Error,
            Reduce(6, 1),
            Error,
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(6),
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Reduce(16, 1),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(16, 1),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Reduce(16, 0),
            Shift(14),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Reduce(8, 1),
            Error,
            Error,
            Reduce(8, 1),
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(8, 1),
            Reduce(8, 1),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(22),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Reduce(10, 0),
            Error,
            Shift(23),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Reduce(4, 1),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(25),
            Error,
            Error,
            Reduce(4, 1),
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Reduce(9, 0),
            Error,
            Reduce(9, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(9, 0),
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Shift(27),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(6),
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(29),
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Shift(31),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(11, 0),
            Reduce(11, 0),
            Reduce(11, 0),
        ],
    ),
    (
        0,
        &[
            Error,
            Reduce(5, 0),
            Error,
            Reduce(5, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(33),
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(35),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Reduce(12, 1),
            Error,
            Error,
            Reduce(12, 1),
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(12, 1),
            Reduce(12, 1),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(14, 0),
            Error,
            Reduce(14, 0),
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(36),
            Error,
            Shift(37),
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(11, 0),
            Reduce(11, 0),
            Reduce(11, 0),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(6),
            Shift(39),
            Shift(40),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Reduce(16, 0),
            Shift(14),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Reduce(9, 1),
            Error,
            Reduce(9, 1),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(9, 1),
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(6),
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Reduce(10, 1),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(45),
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(6),
            Shift(39),
            Shift(40),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(47),
            Reduce(15, 0),
            Error,
            Reduce(15, 0),
            Reduce(15, 0),
            Reduce(15, 0),
            Reduce(15, 0),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(15, 1),
            Error,
            Reduce(15, 1),
            Reduce(15, 1),
            Reduce(15, 1),
            Reduce(15, 1),
        ],
    ),
    (
        0,
        &[
            Reduce(7, 1),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(7, 1),
            Error,
            Error,
            Reduce(7, 1),
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(11, 1),
            Reduce(11, 1),
            Reduce(11, 1),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Shift(48),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Reduce(12, 0),
            Error,
            Error,
            Reduce(12, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(12, 0),
            Reduce(12, 0),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(14, 1),
            Error,
            Reduce(14, 1),
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Reduce(7, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(7, 0),
            Error,
            Error,
            Reduce(7, 0),
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(39),
            Shift(40),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(51),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(17, 0),
            Error,
            Reduce(17, 0),
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(52),
            Error,
            Shift(53),
            Error,
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(6),
            Error,
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(15, 2),
            Error,
            Reduce(15, 2),
            Reduce(15, 2),
            Reduce(15, 2),
            Reduce(15, 2),
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(39),
            Shift(40),
        ],
    ),
    (
        0,
        &[
            Error,
            Reduce(13, 0),
            Error,
            Reduce(13, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(13, 0),
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(17, 1),
            Error,
            Reduce(17, 1),
            Error,
            Error,
            Error,
        ],
    ),
];

fn goto(state: usize, nt: usize) -> usize {
    match (state, nt) {
        (0, 1) => 1,
        (0, 2) => 2,
        (2, 5) => 5,
        (3, 3) => 7,
        (4, 3) => 8,
        (6, 6) => 9,
        (7, 8) => 10,
        (8, 4) => 11,
        (10, 12) => 16,
        (10, 16) => 17,
        (11, 7) => 19,
        (13, 3) => 20,
        (15, 16) => 21,
        (18, 10) => 24,
        (20, 9) => 26,
        (22, 3) => 28,
        (23, 14) => 30,
        (25, 11) => 32,
        (26, 13) => 34,
        (31, 11) => 38,
        (32, 3) => 41,
        (32, 15) => 42,
        (33, 16) => 43,
        (35, 3) => 44,
        (38, 3) => 46,
        (38, 15) => 42,
        (47, 15) => 49,
        (47, 17) => 50,
        (51, 3) => 54,
        (53, 15) => 55,
        _ => unreachable!(),
    }
}

pub enum Symbol {
    /// Constant token
    T(R),
    /// `[_a-zA-Z0-9]+`
    T0_0(Identifier),
    /// `"[^\n"\\]*(\\[^\n][^\n"\\]*)*"`
    T0_1(StringLiteral),
    /// S
    S1(S),
    /// List0<Vocabulary>
    S2(List0<Vocabulary>),
    /// LF
    S3(LF),
    /// List0<SymbolDef>
    S4(List0<SymbolDef>),
    /// Vocabulary
    S5(Vocabulary),
    /// List0<lf>
    S6(List0<R>),
    /// SymbolDef
    S7(SymbolDef),
    /// List0<StringDef>
    S8(List0<StringDef>),
    /// List0<RegexpDef>
    S9(List0<RegexpDef>),
    /// Generics
    S10(Generics),
    /// List0<RuleElement>
    S11(List0<RuleElement>),
    /// StringDef
    S12(StringDef),
    /// RegexpDef
    S13(RegexpDef),
    /// List1<Identifier>
    S14(List1<Identifier>),
    /// RuleElement
    S15(RuleElement),
    /// Ignore
    S16(Ignore),
    /// List1<RuleElement>
    S17(List1<RuleElement>),
}

impl SymbolExt<S, State> for Symbol {
    fn reduce(s: &mut State, s1: &mut Vec<Self>, s2: &mut Vec<usize>, nt: usize, rule: usize) {
        match (nt, rule) {
            (1, 0) => {
                s2.truncate(s2.len() - 4);
                let x = pop_many!(s1, S4, S3, T, S2);
                s1.push(S1(rule1_0(s, x.3, x.2, x.1, x.0)));
            }
            (2, 0) => {
                s1.push(S2(rule2_0(s)));
            }
            (2, 1) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, S5, S2);
                s1.push(S2(rule2_1(s, x.1, x.0)));
            }
            (3, 0) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, S6, T);
                s1.push(S3(rule0_0(s, x.1, x.0)));
            }
            (4, 0) => {
                s1.push(S4(rule2_0(s)));
            }
            (4, 1) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, S7, S4);
                s1.push(S4(rule2_1(s, x.1, x.0)));
            }
            (5, 0) => {
                s2.truncate(s2.len() - 6);
                let x = pop_many!(s1, S9, S3, T, S8, S3, T);
                s1.push(S5(rule4_0(s, x.5, x.4, x.3, x.2, x.1, x.0)));
            }
            (6, 0) => {
                s1.push(S6(rule2_0(s)));
            }
            (6, 1) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, T, S6);
                s1.push(S6(rule2_1(s, x.1, x.0)));
            }
            (7, 0) => {
                s2.truncate(s2.len() - 5);
                let x = pop_many!(s1, S3, S11, T, S10, T0_0);
                s1.push(S7(rule8_0(s, x.4, x.3, x.2, x.1, x.0)));
            }
            (7, 1) => {
                s2.truncate(s2.len() - 4);
                let x = pop_many!(s1, S3, S11, T, S7);
                s1.push(S7(rule8_1(s, x.3, x.2, x.1, x.0)));
            }
            (8, 0) => {
                s1.push(S8(rule2_0(s)));
            }
            (8, 1) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, S12, S8);
                s1.push(S8(rule2_1(s, x.1, x.0)));
            }
            (9, 0) => {
                s1.push(S9(rule2_0(s)));
            }
            (9, 1) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, S13, S9);
                s1.push(S9(rule2_1(s, x.1, x.0)));
            }
            (10, 0) => {
                s1.push(S10(rule9_0(s)));
            }
            (10, 1) => {
                s2.truncate(s2.len() - 3);
                let x = pop_many!(s1, T, S14, T);
                s1.push(S10(rule9_1(s, x.2, x.1, x.0)));
            }
            (11, 0) => {
                s1.push(S11(rule2_0(s)));
            }
            (11, 1) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, S15, S11);
                s1.push(S11(rule2_1(s, x.1, x.0)));
            }
            (12, 0) => {
                s2.truncate(s2.len() - 5);
                let x = pop_many!(s1, S3, T0_1, T, S16, T0_0);
                s1.push(S12(rule6_0(s, x.4, x.3, x.2, x.1, x.0)));
            }
            (12, 1) => {
                s2.truncate(s2.len() - 3);
                let x = pop_many!(s1, S3, T0_1, S16);
                s1.push(S12(rule6_1(s, x.2, x.1, x.0)));
            }
            (13, 0) => {
                s2.truncate(s2.len() - 5);
                let x = pop_many!(s1, S3, T0_1, T, S16, T0_0);
                s1.push(S13(rule7_0(s, x.4, x.3, x.2, x.1, x.0)));
            }
            (14, 0) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, T0_0);
                s1.push(S14(rule3_0(s, x.0)));
            }
            (14, 1) => {
                s2.truncate(s2.len() - 3);
                let x = pop_many!(s1, T0_0, T, S14);
                s1.push(S14(rule3_1(s, x.2, x.1, x.0)));
            }
            (15, 0) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, T0_0);
                s1.push(S15(rule10_0(s, x.0)));
            }
            (15, 1) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, T0_1);
                s1.push(S15(rule10_1(s, x.0)));
            }
            (15, 2) => {
                s2.truncate(s2.len() - 4);
                let x = pop_many!(s1, T, S17, T, T0_0);
                s1.push(S15(rule10_2(s, x.3, x.2, x.1, x.0)));
            }
            (16, 0) => {
                s1.push(S16(rule5_0(s)));
            }
            (16, 1) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, T);
                s1.push(S16(rule5_1(s, x.0)));
            }
            (17, 0) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, S15);
                s1.push(S17(rule3_0(s, x.0)));
            }
            (17, 1) => {
                s2.truncate(s2.len() - 3);
                let x = pop_many!(s1, S15, T, S17);
                s1.push(S17(rule3_1(s, x.2, x.1, x.0)));
            }
            _ => unreachable!(),
        }
    }

    fn reduce_regexp(
        s: &mut State,
        s1: &mut Vec<Self>,
        mode: usize,
        id: usize,
        text: String,
        range: R,
    ) {
        match (mode, id) {
            (0, 11) => s1.push(T0_0(regex0_0(s, range, text))),
            (0, 12) => s1.push(T0_1(regex0_1(s, range, text))),
            _ => s1.push(T(range)),
        }
    }

    fn get_s(self) -> Option<S> {
        match self {
            S1(s) => Some(s),
            _ => None,
        }
    }
}
