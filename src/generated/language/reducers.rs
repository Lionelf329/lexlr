use crate::R;

// Identifier
pub fn regex0_0(_: &mut State, _: R, s: String) -> Identifier {
    s
}

// StringLiteral
pub fn regex0_1(_: &mut State, _: R, s: String) -> StringLiteral {
    let mut escaped = false;
    let mut ans = String::new();
    for c in s[1..s.len() - 1].chars() {
        if escaped {
            match c {
                'n' => ans.push('\n'),
                'r' => ans.push('\r'),
                't' => ans.push('\t'),
                c => ans.push(c),
            }
            escaped = false;
        } else if c == '\\' {
            escaped = true;
        } else {
            ans.push(c);
        }
    }
    ans
}

// LF -> lf List0<lf>
pub fn rule0_0(_: &mut State, _: R, _: List0<R>) -> LF {
    ()
}

// S -> List0<Vocabulary> [symbols] LF List0<SymbolDef>
pub fn rule1_0(
    _: &mut State,
    vocabularies: List0<Vocabulary>,
    _: R,
    _: LF,
    symbols: List0<SymbolDef>,
) -> S {
    S {
        vocabularies,
        symbols,
    }
}

// List0<T> -> ~epsilon
pub fn rule2_0<T>(_: &mut State) -> List0<T> {
    Vec::new()
}

// List0<T> -> List0<T> T
pub fn rule2_1<T>(_: &mut State, mut list: List0<T>, e: T) -> List0<T> {
    list.push(e);
    list
}

// List1<T> -> T
pub fn rule3_0<T>(_: &mut State, e: T) -> List1<T> {
    vec![e]
}

// List1<T> -> List1<T> , T
pub fn rule3_1<T>(_: &mut State, mut list: List1<T>, _: R, e: T) -> List1<T> {
    list.push(e);
    list
}

// Vocabulary -> [strings] LF List0<StringDef> [regexps] LF List0<RegexpDef>
pub fn rule4_0(
    _: &mut State,
    _: R,
    _: LF,
    strings: List0<StringDef>,
    _: R,
    _: LF,
    regexps: List0<RegexpDef>,
) -> Vocabulary {
    Vocabulary { strings, regexps }
}

// Ignore -> ~epsilon
pub fn rule5_0(_: &mut State) -> Ignore {
    false
}

// Ignore -> ~
pub fn rule5_1(_: &mut State, _: R) -> Ignore {
    true
}

// StringDef -> Identifier Ignore = StringLiteral LF
pub fn rule6_0(
    _: &mut State,
    name: Identifier,
    ignored: Ignore,
    _: R,
    text: StringLiteral,
    _: LF,
) -> StringDef {
    StringDef {
        name,
        text,
        ignored,
    }
}

// StringDef -> Ignore StringLiteral LF
pub fn rule6_1(_: &mut State, ignored: Ignore, text: StringLiteral, _: LF) -> StringDef {
    StringDef {
        name: text.clone(),
        text,
        ignored,
    }
}

// RegexpDef -> Identifier Ignore = StringLiteral LF
pub fn rule7_0(
    _: &mut State,
    name: Identifier,
    ignored: Ignore,
    _: R,
    text: StringLiteral,
    _: LF,
) -> RegexpDef {
    RegexpDef {
        name,
        text,
        ignored,
    }
}

// SymbolDef -> Identifier Generics = List0<RuleElement> LF
pub fn rule8_0(
    _: &mut State,
    name: Identifier,
    generics: Generics,
    _: R,
    rule: List0<RuleElement>,
    _: LF,
) -> SymbolDef {
    SymbolDef {
        name,
        generics,
        rules: [(0, rule)].into(),
    }
}

// SymbolDef -> SymbolDef | List0<RuleElement> LF
pub fn rule8_1(
    _: &mut State,
    mut nt: SymbolDef,
    _: R,
    rule: List0<RuleElement>,
    _: LF,
) -> SymbolDef {
    nt.rules.insert(nt.rules.len(), rule);
    nt
}

// Generics -> ~epsilon
pub fn rule9_0(_: &mut State) -> Generics {
    Vec::new()
}

// Generics -> < List1<Identifier> >
pub fn rule9_1(_: &mut State, _: R, list: List1<Identifier>, _: R) -> Generics {
    list
}

// RuleElement -> Identifier
pub fn rule10_0(_: &mut State, name: Identifier) -> RuleElement {
    RuleElement::new(name)
}

// RuleElement -> StringLiteral
pub fn rule10_1(_: &mut State, s: StringLiteral) -> RuleElement {
    RuleElement::new(s)
}

// RuleElement -> Identifier < List1<RuleElement> >
pub fn rule10_2(
    _: &mut State,
    name: Identifier,
    _: R,
    params: List1<RuleElement>,
    _: R,
) -> RuleElement {
    RuleElement { name, params }
}

pub type State = ();
pub type Identifier = String;
pub type StringLiteral = String;
pub type LF = ();
pub type S = crate::Language<String>;
pub type List0<T> = Vec<T>;
pub type List1<T> = Vec<T>;
pub type Vocabulary = crate::Vocabulary;
pub type Ignore = bool;
pub type StringDef = crate::Terminal;
pub type RegexpDef = crate::Terminal;
pub type SymbolDef = crate::GenericNT<String>;
pub type Generics = Vec<String>;
pub type RuleElement = crate::RuleElement<String>;
