/// Enables iterators of string-like values to be joined using a provided delimiter.
pub trait JoinExt {
    /// Concatenate the values into a `String`, separated by the provided delimiter.
    fn join(self, s: &str) -> String;
}

impl<I: Iterator<Item = T>, T: Into<String>> JoinExt for I {
    fn join(self, s: &str) -> String {
        self.map(|x| x.into()).collect::<Vec<_>>().join(s)
    }
}

#[cfg(test)]
mod tests {
    use super::JoinExt;

    #[test]
    pub fn test_join() {
        let arr1: &[&str] = &[];
        let arr2: &[&str] = &["a"];
        let arr3: &[&str] = &["a", "b"];
        assert_eq!(arr1.iter().copied().join(", "), "");
        assert_eq!(arr2.iter().copied().join(", "), "a");
        assert_eq!(arr3.iter().copied().join(", "), "a, b");
    }
}
