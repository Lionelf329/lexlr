pub use insert_many::InsertMany;
pub use join_ext::JoinExt;
pub use key_set::KeySet;
pub use map_ext::BTreeMapExt;
pub use map_inner::MapInner;
pub use map_of_set::MapOfSet;
pub use partition_by_key::PartitionByKey;

mod insert_many;
mod join_ext;
mod key_set;
mod map_ext;
mod map_inner;
mod map_of_set;
mod partition_by_key;

pub fn color(string: &str, color: usize) -> String {
    format!("\x1b[{}m{}\x1b[0m", color, string)
}
