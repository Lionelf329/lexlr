use std::collections::BTreeMap;

pub trait BTreeMapExt<K, V> {
    fn map_keys<K2: Ord, F: FnMut(K) -> K2>(self, f: F) -> BTreeMap<K2, V>;

    fn filter_map_keys<K2: Ord, F: FnMut(K) -> Option<K2>>(self, f: F) -> BTreeMap<K2, V>;

    fn map_values<V2, F: FnMut(V) -> V2>(self, f: F) -> BTreeMap<K, V2>;

    fn filter_map_values<V2, F: FnMut(V) -> Option<V2>>(self, f: F) -> BTreeMap<K, V2>;
}

impl<K: Ord, V> BTreeMapExt<K, V> for BTreeMap<K, V> {
    fn map_keys<K2: Ord, F: FnMut(K) -> K2>(self, mut f: F) -> BTreeMap<K2, V> {
        self.into_iter().map(|(k, v)| (f(k), v)).collect()
    }

    fn filter_map_keys<K2: Ord, F: FnMut(K) -> Option<K2>>(self, mut f: F) -> BTreeMap<K2, V> {
        self.into_iter()
            .filter_map(|(k, v)| f(k).map(|k| (k, v)))
            .collect()
    }

    fn map_values<V2, F: FnMut(V) -> V2>(self, mut f: F) -> BTreeMap<K, V2> {
        self.into_iter().map(|(k, v)| (k, f(v))).collect()
    }

    fn filter_map_values<V2, F: FnMut(V) -> Option<V2>>(self, mut f: F) -> BTreeMap<K, V2> {
        self.into_iter()
            .filter_map(|(k, v)| f(v).map(|v| (k, v)))
            .collect()
    }
}
