use std::collections::{BTreeMap, BTreeSet};

/// Adds a method on [BTreeMap] instances to create a set of their keys.
pub trait KeySet<T> {
    fn key_set(&self) -> BTreeSet<T>;
}

impl<T: Copy + Ord, Value> KeySet<T> for BTreeMap<T, Value> {
    fn key_set(&self) -> BTreeSet<T> {
        self.keys().copied().collect()
    }
}
