use lexlr::{write, Language, OwnedParserData, ParseWrapper};
use std::fs::read_to_string;
use std::path::Path;

pub fn main() {
    let args: Vec<String> = std::env::args().skip(1).collect();
    let args: Vec<&str> = args.iter().map(|x| &x[..]).collect();
    if args[0] == "rebuild_self" {
        let language = Language::new(include_str!("generated/language/grammar.txt"));
        let (language, errors) = language.resolve_refs();
        assert!(errors.no_errors());
        let data = OwnedParserData::new(&language);
        write(&data, "src/generated/language", true);
        let language = Language::new(include_str!("generated/regex/grammar.txt"));
        let (language, errors) = language.resolve_refs();
        assert!(errors.no_errors());
        let data = OwnedParserData::new(&language);
        write(&data, "src/generated/regex", true);
    } else if args[0] == "build" {
        let file = read_to_string(Path::new(&args[1])).unwrap();
        let language = Language::new(&file);
        let (language, errors) = language.resolve_refs();
        assert!(errors.no_errors());
        let data = OwnedParserData::new(&language);
        write(&data, &args[2], false);
    } else if args[0] == "test" {
        let file = read_to_string(Path::new(&args[1])).unwrap();
        let language = Language::new(&file);
        let (language, errors) = language.resolve_refs();
        assert!(errors.no_errors());
        let data = OwnedParserData::new(&language);
        let input = read_to_string(Path::new(&args[2])).unwrap();
        let parse = input.chars().parse(&data);
        match parse {
            Ok(parse) => println!("{}", parse),
            Err(range) => println!(
                "Error at: {}",
                input
                    .chars()
                    .skip(range.start)
                    .take(range.end - range.start)
                    .collect::<String>()
            ),
        };
    }
}
