use std::ops::Range;

/// A position in a parse input.
#[derive(Copy, Clone)]
pub struct R {
    /// The lower bound of the range (inclusive).
    pub start: usize,
    /// The upper bound of the range (exclusive).
    pub end: usize,
}

impl R {
    /// Creates a [`Range<usize>`] with the same start and end.
    pub fn range(self) -> Range<usize> {
        self.start..self.end
    }
}

impl From<Range<usize>> for R {
    fn from(value: Range<usize>) -> Self {
        Self {
            start: value.start,
            end: value.end,
        }
    }
}

impl std::fmt::Debug for R {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.range(), f)
    }
}
