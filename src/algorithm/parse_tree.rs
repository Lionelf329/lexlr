use crate::{color, Language, Parser, SymbolExt, SymbolRef, R};
use std::fmt::{Debug, Display, Formatter, Result};
use ParseTree::*;

/// Represents a parse tree for a particular input string.
#[derive(Eq, PartialEq, Ord, PartialOrd)]
pub enum ParseTree {
    /// A terminal in the AST.
    T(String),
    /// A nonterminal in the AST.
    NT(String, Vec<ParseTree>),
}

impl Debug for ParseTree {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match self {
            T(x) => f.write_str(&format!("{}", x)),
            NT(nt, children) => {
                if children.len() != 1 {
                    let strings: Vec<_> = children.iter().map(|x| format!("{:?}", x)).collect();
                    f.write_str(&format!("{}({})", nt, strings.join(", ")))
                } else {
                    f.write_str(&format!("{:?}", children[0]))
                }
            }
        }
    }
}

impl Display for ParseTree {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        self.display(f, 0)
    }
}

impl ParseTree {
    fn display(&self, f: &mut Formatter<'_>, depth: usize) -> Result {
        match self {
            T(x) => f.write_str(&color(x, 32)),
            NT(nt, children) => {
                if children.len() == 0 {
                    f.write_str(&format!("{}()", nt))
                } else if children.len() == 1 {
                    f.write_str(&format!("{}(", nt))?;
                    children[0].display(f, depth)?;
                    f.write_str(")")
                } else {
                    f.write_str(&format!("{}(\n", color(nt, 35)))?;
                    for c in children {
                        f.write_str(&"  ".repeat(depth + 1))?;
                        c.display(f, depth + 1)?;
                        f.write_str("\n")?;
                    }
                    f.write_str(&"  ".repeat(depth))?;
                    f.write_str(")")
                }
            }
        }
    }
}

impl SymbolExt<ParseTree, ParseTreeState<'_>> for ParseTree {
    fn reduce(
        state: &mut ParseTreeState,
        s1: &mut Vec<Self>,
        s2: &mut Vec<usize>,
        nt: usize,
        rule: usize,
    ) {
        let nt = &state.parser.nonterminals[nt];
        let name = nt.name.describe(&[], state.language);
        let len = nt.rules[&rule].len();
        let args = s1.split_off(s1.len() - len);
        s2.truncate(s2.len() - len);
        s1.push(NT(name, args));
    }

    fn reduce_regexp(
        _: &mut ParseTreeState,
        s1: &mut Vec<Self>,
        _mode: usize,
        _id: usize,
        text: String,
        _: R,
    ) {
        s1.push(T(text));
    }

    fn get_s(self) -> Option<ParseTree> {
        Some(self)
    }
}

#[derive(Debug)]
pub struct ParseTreeState<'a> {
    pub language: &'a Language<SymbolRef>,
    pub parser: &'a Parser<'a>,
}
