use crate::{Action, Language, Lexer, ParseTree, Parser, ParserData, SymbolRef, Token};

/// Contains all the data needed to use the generated DFA and LALR tables.
#[derive(Debug)]
pub struct OwnedParserData<'a> {
    /// The language used to create the parser data
    pub language: &'a Language<SymbolRef>,
    /// The dfa
    pub lexer: Lexer,
    /// The lalr(1) parse tables
    pub parser: Parser<'a>,
    /// The tokens that are actually used for each lexer mode.
    pub mapping: Vec<Vec<Token>>,
}

impl<'a> OwnedParserData<'a> {
    /// Returns an `OwnedParserData` constructed from a language.
    pub fn new(language: &'a Language<SymbolRef>) -> Self {
        Self {
            language,
            lexer: Lexer::new(language).unwrap(),
            parser: Parser::new(language),
            mapping: language.get_mapping(),
        }
    }
}

impl ParserData<ParseTree> for OwnedParserData<'_> {
    fn lexer_transition(&self, mode: usize, state: usize, char: char) -> Option<usize> {
        self.lexer.transition(mode, state, char)
    }

    fn lexer_label(&self, mode: usize, state: usize) -> Option<(Token, Option<usize>)> {
        self.lexer
            .label(mode, state)
            .map(|token| (token, self.mapping[mode].iter().position(|&m| m == token)))
    }

    fn lexer_mode(&self, state: usize) -> usize {
        self.parser.states[state].lexer.unwrap_or(0)
    }

    fn get_action(&self, state: usize, index: usize) -> Action {
        self.parser.states[state].action(self.mapping[self.lexer_mode(state)][index])
    }

    fn get_goto(&self, state: usize, index: usize) -> usize {
        self.parser.states[state].goto[index].unwrap()
    }
}
