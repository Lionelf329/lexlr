use crate::{char_to_index, Automata, Dfa};

impl<T> Automata<usize, T> for Dfa<T> {
    fn start_state(&self) -> usize {
        0
    }

    fn transition(&self, state: &usize, char: char) -> Option<usize> {
        self.states[*state].1[char_to_index(char)]
    }

    fn label(&self, state: &usize) -> Option<&T> {
        self.states[*state].0.as_ref()
    }
}
