/// The number of unique characters recognized by the lexer.
pub const ALPHABET_SIZE: usize = 99;

/// The set of unique characters accepted by the lexer.
pub const ALPHABET: [char; ALPHABET_SIZE] = [
    '\t', '\n', '\r', ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.',
    '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A',
    'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
    'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
    'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '{', '|', '}', '~', UNICODE,
];

/// To avoid having to deal with the complexities of unicode, all characters outside the normal
/// ascii range are transformed into this one special character, 0x7f. This value was chosen
/// because it is one greater than the ascii code of the last printable character.
pub const UNICODE: char = '\x7f';

/// Returns the index of the character within the alphabet, converting any unknown character to the
/// [UNICODE] constant.
pub fn char_to_index(char: char) -> usize {
    match char {
        '\t' => 0,
        '\n' => 1,
        '\r' => 2,
        _ => {
            if ' ' <= char && char <= '~' {
                char as usize - 29
            } else {
                98
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{char_to_index, ALPHABET, ALPHABET_SIZE, UNICODE};

    #[test]
    fn test_alphabet() {
        // Check that the first 3 characters are the common whitespace characters
        assert_eq!(ALPHABET[0], '\t');
        assert_eq!(ALPHABET[1], '\n');
        assert_eq!(ALPHABET[2], '\r');

        // Check that all the remaining characters have contiguous encodings
        for i in 3..ALPHABET.len() {
            assert_eq!(ALPHABET[i], (i + 29) as u8 as char);
        }

        // Check that the unicode constant is right after all the printable characters
        assert_eq!(ALPHABET.last().copied().unwrap(), UNICODE);

        // Check that the alphabet size constant is correct
        assert_eq!(ALPHABET.len(), ALPHABET_SIZE)
    }

    #[test]
    fn test_char_to_index() {
        // Check that char_to_index maps each character in the alphabet back to the right index
        for (i, c) in ALPHABET.iter().copied().enumerate() {
            assert_eq!(i, char_to_index(c));
        }

        // Check that unknown characters are mapped to the unicode constant
        assert_eq!(UNICODE, ALPHABET[char_to_index(0xff as char)]);
    }
}
