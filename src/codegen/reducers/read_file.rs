use std::collections::BTreeMap;
use std::fs::File;
use std::io::Read;
use std::path::Path;

pub fn read_file(
    path: &str,
) -> (
    BTreeMap<String, (Vec<(String, String)>, String)>,
    BTreeMap<String, String>,
) {
    let mut s = String::new();
    if let Ok(mut file) = File::open(Path::new(path)) {
        file.read_to_string(&mut s).unwrap();
    } else {
        return (BTreeMap::new(), BTreeMap::new());
    }
    let mut methods: Vec<(&str, Vec<&str>, bool)> = Vec::new();
    let mut types = Vec::new();
    for line in s.lines() {
        if let Some((_comment, lines, true)) = methods.last_mut() {
            if line == "}" {
                methods.last_mut().unwrap().2 = false;
            } else {
                lines.push(line);
            }
        } else if line.starts_with("//") {
            methods.push((&line[3..], Vec::new(), true));
        } else if line.starts_with("pub type") {
            types.push(line);
        }
    }
    let methods = methods
        .into_iter()
        .map(|(comment, lines, _)| {
            let method = lines.join("\n");
            let i = method.find("{").unwrap();
            let header = &method[..=i];
            let body = &method[i + 2..];
            (
                comment.to_string(),
                (parse_header(header), body.to_string()),
            )
        })
        .collect();
    let types = types
        .into_iter()
        .map(|line| {
            let mut line = line[9..line.len() - 1].split(" = ");
            (
                line.next().unwrap().to_string(),
                line.next().unwrap().to_string(),
            )
        })
        .collect();
    (methods, types)
}

/// Returns the names and types of each parameter to the function.
fn parse_header(mut header: &str) -> Vec<(String, String)> {
    header = &header[header.find('(').unwrap() + 1..header.rfind(')').unwrap()];
    let mut parts = Vec::new();
    let mut i = 0;
    let mut stack = 0;
    for j in 0..header.len() {
        let byte = header.as_bytes()[j];
        match byte {
            b'(' | b'{' | b'[' | b'<' => {
                stack += 1;
            }
            b')' | b'}' | b']' | b'>' => {
                stack -= 1;
            }
            b':' | b',' => {
                if stack == 0 {
                    let str = String::from(header[i..j].trim());
                    i = j + 1;
                    if byte == b':' {
                        parts.push((str, String::new()));
                    } else {
                        parts.last_mut().unwrap().1 = str;
                    }
                }
            }
            _ => (),
        }
    }
    if parts.last().unwrap().1.is_empty() {
        parts.last_mut().unwrap().1 = String::from(header[i..].trim());
    }
    parts
}
