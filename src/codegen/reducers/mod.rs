use crate::{JoinExt, OwnedParserData};
use read_file::read_file;
use reducer::{Reducer, ReducerType};

mod read_file;
mod reducer;

pub fn reducers(data: &OwnedParserData, crate_name: &str, path: &str) -> String {
    let (mut old_reducers, mut old_types) = read_file(&format!("{}/reducers.rs", path));
    let mut templates = Vec::new();
    let mut types = Vec::new();
    types.push((
        "State".into(),
        old_types.remove("State").unwrap_or(String::from("()")),
    ));
    for (i, j, r) in data.language.regexps() {
        types.push((
            r.name.to_string(),
            old_types.remove(&r.name).unwrap_or(String::from("String")),
        ));
        let (old_params, body) = old_reducers
            .remove(&r.name)
            .unwrap_or_else(|| (Vec::new(), "    s".into()));
        templates.push(Reducer {
            comment: r.name.clone(),
            reducer_type: ReducerType::Regexp(i, j),
            generics: Vec::new(),
            params: make_params(
                ["R", "String"].iter().copied().map(str::to_string),
                &old_params,
            ),
            return_type: r.name.clone(),
            body,
        });
    }
    for (i, s) in data.language.symbols.iter().enumerate() {
        let generic_string = if s.generics.is_empty() {
            String::new()
        } else {
            format!("{}", s.generics.join(", "))
        };
        let (name, mut val) = if s.generics.is_empty() {
            (s.name.to_string(), String::from("()"))
        } else if s.generics.len() == 1 {
            (
                format!("{}<{}>", s.name, generic_string),
                format!("Vec<{}>", generic_string),
            )
        } else {
            (
                format!("{}<{}>", s.name, generic_string),
                format!("Vec<({})>", generic_string),
            )
        };
        val = old_types.remove(&name).unwrap_or(val);
        types.push((name, val));
        for (&j, r) in s.rules.iter() {
            let mut rule = r
                .iter()
                .map(|x| x.describe(&s.generics, data.language))
                .join(" ");
            if rule.is_empty() {
                rule.push_str("~epsilon");
            }
            let return_type = if s.generics.is_empty() {
                s.name.clone()
            } else {
                format!("{}<{}>", s.name, generic_string)
            };
            let comment = format!("{} -> {}", return_type, rule);
            let (old_params, body) = old_reducers.remove(&comment).unwrap_or_else(|| {
                (
                    Vec::new(),
                    if s.generics.is_empty() {
                        "    ()".into()
                    } else {
                        "    Vec::new()".into()
                    },
                )
            });
            let params = make_params(
                r.iter().map(|e| e.type_str(&s.generics, data.language)),
                &old_params,
            );
            templates.push(Reducer {
                comment,
                reducer_type: ReducerType::Symbol(i, j),
                generics: s.generics.clone(),
                params,
                return_type,
                body,
            });
        }
    }
    format!(
        "use {}::R;\n\n{}\n\n{}\n",
        crate_name,
        templates.iter().map(ToString::to_string).join("\n\n"),
        types
            .iter()
            .map(|x| format!("pub type {} = {};", x.0, x.1))
            .join("\n")
    )
}

fn make_params<T: Iterator<Item = String>>(
    iterator: T,
    mut old_params: &[(String, String)],
) -> Vec<(String, String)> {
    iterator
        .filter(|e| e != "()")
        .map(|e| {
            (
                match old_params.iter().position(|(_, t)| t == &e) {
                    None => "_".into(),
                    Some(i) => {
                        let name = old_params[i].0.clone();
                        old_params = &old_params[i + 1..];
                        name
                    }
                },
                e.to_string(),
            )
        })
        .collect()
}
