/// A position in a grammar rule.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct Item {
    /// The index of the nonterminal that is being parsed.
    pub nt: usize,
    /// The index of the rule within that nonterminal.
    pub r: usize,
    /// The number of rule elements that have already been parsed.
    pub i: usize,
}

impl Item {
    /// Creates a new item.
    pub fn new(nt: usize, r: usize) -> Self {
        Self { nt, r, i: 0 }
    }

    /// Creates a copy where the parsing has progressed by one symbol.
    pub fn next(&self) -> Self {
        Item {
            nt: self.nt,
            r: self.r,
            i: self.i + 1,
        }
    }
}
