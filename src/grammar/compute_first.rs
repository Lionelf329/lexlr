use crate::{Grammar, MapOfSet, SymbolID, Token};
use std::collections::{BTreeMap, BTreeSet, VecDeque};

impl Grammar {
    /// Determines which terminals each nonterminal can start with
    pub(crate) fn compute_first(&mut self) {
        // The values here mean that [key] can be the first NT in [value]. All values are NT IDs.
        let mut is_start_of = BTreeMap::new();

        // each tuple is (nt_id, t_id), which is a terminal that needs to be propagated
        let mut tuples = VecDeque::new();

        // Get the list of epsilons because it makes it easier to deal with the borrow checker
        let eps: Vec<bool> = self.iter().map(|x| x.epsilon).collect();

        // For each rule in each nonterminal
        for (nt_id, nt) in self.iter_mut().enumerate() {
            for rule in nt.rules.values() {
                // For each element, until one of them can't be empty
                for &element in rule {
                    match element {
                        // If the element is a terminal, add it to the "first" set and then break
                        SymbolID::T(t_id) => {
                            nt.first.insert(t_id);
                            tuples.push_back((nt_id, t_id));
                            break;
                        }
                        // Otherwise, keep track of that the element can be the beginning of the NT.
                        // If it can't be empty, don't look any further in this rule.
                        SymbolID::NT(element_id) => {
                            is_start_of.add(element_id, nt_id);
                            if !eps[element_id] {
                                break;
                            }
                        }
                    }
                }
            }
        }
        // While there are dependencies that haven't been propagated, take the first one
        while let Some((nt, t)) = tuples.pop_front() {
            // If there is anywhere to propagate to
            if let Some(next_nts) = is_start_of.get(&nt) {
                // For each place it can be propagated
                for &next_nt in next_nts {
                    // Add it, and if it wasn't already there, then put it back in
                    // the list to propagate further
                    if self[next_nt].first.insert(t) {
                        tuples.push_back((next_nt, t));
                    }
                }
            }
        }
    }

    /// Returns the set of tokens that this symbol can start with.
    ///
    /// If it is a terminal, just return a set containing itself. If it is a nonterminal, return
    /// the `first` set.
    pub fn first(&self, symbol: SymbolID) -> BTreeSet<Token> {
        match symbol {
            SymbolID::T(id) => BTreeSet::from([id]),
            SymbolID::NT(id) => self[id].first.clone(),
        }
    }
}
