use crate::{Item, Language, MapInner, RuleElement, SymbolRef, Token};
use std::ops::{Index, IndexMut};

pub use types::*;

mod closure;
mod combined_states;
mod compute_epsilons;
mod compute_first;
mod process_state;
mod types;

#[cfg(test)]
mod tests;

/// An augmented context free grammar with no generics.
#[derive(Debug, Eq, PartialEq)]
pub struct Grammar(Vec<NonTerminal>);

impl Grammar {
    fn allocate_nt_ifndef(&mut self, name: &RuleElement<SymbolRef>) -> usize {
        if let Some(i) = self.iter().position(|nt| &nt.name == name) {
            i
        } else {
            self.0.push(name.into());
            self.len() - 1
        }
    }

    /// Returns an iterator over the nonterminals in the grammar.
    pub fn iter(&self) -> std::slice::Iter<NonTerminal> {
        self.0.iter()
    }

    /// Returns an iterator that allows modifying each nonterminal.
    fn iter_mut(&mut self) -> std::slice::IterMut<NonTerminal> {
        self.0.iter_mut()
    }

    /// Returns the number of nonterminals in the grammar.
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Gets the next element in the rule referenced by the specified item.
    pub fn next_element(&self, item: Item) -> Option<SymbolID> {
        self[item.nt].rules[&item.r].get(item.i).copied()
    }
}

impl Index<usize> for Grammar {
    type Output = NonTerminal;

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl IndexMut<usize> for Grammar {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

impl<'a> IntoIterator for &'a Grammar {
    type Item = &'a NonTerminal;
    type IntoIter = std::slice::Iter<'a, NonTerminal>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

/// An enumeration representing all the ways that expanding generics can go wrong.
#[derive(Debug)]
pub enum GenericExpansionError {
    /// The start symbol "S" was not defined.
    MissingStartSymbol,
    /// There are some nonterminals that have no derivations due to cyclic references.
    CannotComputeEpsilons(Vec<RuleElement<SymbolRef>>),
}

const START_SYMBOL: &str = "S";

impl Language<SymbolRef> {
    /// Returns a list of nonterminal definitions that form an equivalent grammar with no generics.
    ///
    /// The new grammar retains the necessary information to upgrade it back to the original
    /// definitions. It also computes information such as whether each nonterminal can be matched
    /// by empty input, which terminals they can start with, and which terminals they can be
    /// followed by.
    pub fn nonterminals(&self) -> Result<Grammar, GenericExpansionError> {
        let mut grammar = Grammar(vec![NonTerminal::new(
            RuleElement::new(SymbolRef::StartSymbol),
            [(0, vec![SymbolID::NT(1), SymbolID::T(Token::EOF)])].into(),
        )]);

        // Add first item to the queue
        let i = self
            .symbols
            .iter()
            .position(|x| x.name == START_SYMBOL)
            .ok_or(GenericExpansionError::MissingStartSymbol)?;
        grammar.allocate_nt_ifndef(&RuleElement::new(SymbolRef::Symbol(i)));

        // Add NTs from the queue to the closure until the queue is empty
        for nt_id in 1.. {
            if nt_id >= grammar.len() {
                break;
            }
            let RuleElement { name, params } = &grammar[nt_id].name;
            let nt_def = &self.symbols[name.symbol().unwrap()];
            // get the definition for the current nonterminal
            let nt_rules = nt_def.replace_generics(&params);

            // format the rules based on their generic parameters, adding nonterminals to the queue
            // if they haven't been added yet
            let rules = nt_rules.map_inner(|e| match e.name {
                SymbolRef::String(x, y) => SymbolID::T(Token::String(x, y)),
                SymbolRef::Regexp(x, y) => SymbolID::T(Token::Regexp(x, y)),
                SymbolRef::Symbol(_) => SymbolID::NT(grammar.allocate_nt_ifndef(e)),
                SymbolRef::StartSymbol | SymbolRef::Generic(_) => unreachable!(),
            });

            // add the nonterminal definition to the closure
            assert!(grammar[nt_id].rules.is_empty());
            grammar[nt_id].rules = rules;
        }

        grammar.compute_epsilons()?;
        grammar.compute_first();
        Ok(grammar)
    }
}
