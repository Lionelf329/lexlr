use crate::{Follow, Grammar, InsertMany, Item, KeySet, SymbolID, Token};
use std::collections::{BTreeMap, BTreeSet};

impl Grammar {
    pub(crate) fn closure(
        &self,
        core_items: &BTreeMap<Item, BTreeSet<Token>>,
    ) -> BTreeMap<Item, BTreeSet<Token>> {
        self.generic_closure(core_items.clone())
    }

    #[allow(unused)]
    pub(crate) fn closure_map(
        &self,
        core_items: &BTreeSet<Item>,
    ) -> BTreeMap<Item, BTreeSet<Follow>> {
        self.generic_closure(
            core_items
                .iter()
                .map(|&x| (x, BTreeSet::from([Follow::Item(x)])))
                .collect(),
        )
    }

    fn generic_closure<T: Copy + Ord + From<Token>>(
        &self,
        mut closure: BTreeMap<Item, BTreeSet<T>>,
    ) -> BTreeMap<Item, BTreeSet<T>> {
        // queue of item indices to try to expand
        let mut to_process = closure.key_set();

        // For each unfinished item index, while there are any left
        while let Some(item) = to_process.pop_first() {
            // If there is a nonterminal next in the rule
            if let Some((&SymbolID::NT(next_element), remaining)) =
                self[item.nt].rules[&item.r][item.i..].split_first()
            {
                let follow = self.follow(remaining, &closure[&item]);

                // For each rule
                for rule_index in 0..self[next_element].rules.len() {
                    // Create the corresponding item at the beginning of the rule
                    let new_item = Item::new(next_element, rule_index);

                    // If the item is already in the closure
                    if let Some(existing_follow) = closure.get_mut(&new_item) {
                        // Add the new tokens to the follow set if they're missing, and mark the
                        // item for reprocessing if anything wasn't already there
                        if existing_follow.insert_many(&follow) {
                            to_process.insert(new_item);
                        }
                    } else {
                        // If the item isn't in the closure yet, add it and mark it for processing
                        closure.insert(new_item, follow.clone());
                        to_process.insert(new_item);
                    }
                }
            }
        }
        closure
    }

    /// Finds all terminals that can appear next in this rule, starting with the remainder of
    /// the rule, and if relevant, including the follow set for the item.
    fn follow<T: Copy + Ord + From<Token>>(
        &self,
        remaining_elements: &[SymbolID],
        follow: &BTreeSet<T>,
    ) -> BTreeSet<T> {
        let mut result = BTreeSet::new();
        for &e in remaining_elements {
            for x in self.first(e) {
                result.insert(x.into());
            }
            if !self.epsilon(e) {
                return result;
            }
        }
        for &x in follow {
            result.insert(x);
        }
        result
    }
}
