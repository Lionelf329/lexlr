use crate::{GenericExpansionError, Grammar, SymbolID};

impl Grammar {
    /// Determines which nonterminals can be matched by epsilon
    pub(crate) fn compute_epsilons(&mut self) -> Result<(), GenericExpansionError> {
        // The algorithm is to start by assuming that all terminals are not empty, and all NTs are
        // unknown. Then, assume every NT with a rule that is entirely epsilons is an epsilon, and
        // every NT where every rule has an element with no epsilons is not an epsilon. Stop when it
        // categorizes everything, or when progress is no longer made.
        let mut undecided = self.len();
        let mut decided = vec![false; undecided];
        while undecided > 0 {
            let prev_undecided = undecided;
            for i in 0..self.len() {
                if !decided[i] {
                    if self[i].rules.values().all(|rule| {
                        rule.iter().any(|&element| match element {
                            SymbolID::T(_) => true,
                            SymbolID::NT(j) => decided[j] && !self[j].epsilon,
                        })
                    }) {
                        self[i].epsilon = false;
                        decided[i] = true;
                        undecided -= 1;
                    }
                    if self[i].rules.values().any(|rule| {
                        rule.iter().all(|&element| match element {
                            SymbolID::T(_) => false,
                            SymbolID::NT(j) => decided[j] && self[j].epsilon,
                        })
                    }) {
                        self[i].epsilon = true;
                        decided[i] = true;
                        undecided -= 1;
                    }
                }
            }
            if undecided == prev_undecided {
                let elements: Vec<_> = Iterator::zip(decided.iter(), self.iter())
                    .filter(|(&decided, _)| !decided)
                    .map(|(_, nt)| nt.name.clone())
                    .collect();
                return Err(GenericExpansionError::CannotComputeEpsilons(elements));
            }
        }
        Ok(())
    }

    /// Returns `true` if the symbol can be matched by empty input.
    ///
    /// If it is a terminal, return false. Otherwise, return the previously calculated answer.
    pub fn epsilon(&self, symbol: SymbolID) -> bool {
        match symbol {
            SymbolID::T(_) => false,
            SymbolID::NT(id) => self[id].epsilon,
        }
    }
}
