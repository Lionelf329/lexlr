use crate::{Follow, Item, SymbolID};
use std::collections::{BTreeMap, BTreeSet};

/// The information associated with a set of items, which corresponds to exactly one state in the
/// LALR(1) tables and potentially several states in the canonical LR(1) tables.
#[derive(Debug, Default)]
pub struct StateInfo {
    /// The item sets that lead into the current item set.
    pub prev: BTreeSet<BTreeSet<Item>>,
    /// The item sets that follow the current item set.
    pub next: BTreeMap<SymbolID, BTreeMap<Item, BTreeSet<Follow>>>,
    /// The set of terminals and item follow sets that cannot be in the follow set for each item.
    pub follow_exclusions: BTreeSet<[Item; 2]>,
}
