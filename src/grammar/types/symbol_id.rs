use crate::Token;

/// A lightweight identifier of any terminal or nonterminal in the language.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum SymbolID {
    /// A terminal
    T(Token),
    /// A nonterminal
    NT(usize),
}
