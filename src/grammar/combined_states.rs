use crate::{Action, BTreeMapExt, Follow, Grammar, InsertMany, Item, KeySet, MapOfSet, StateInfo};
use std::cmp::Ordering;
use std::collections::{BTreeMap, BTreeSet, VecDeque};

impl Grammar {
    pub(crate) fn combined_states(
        &self,
    ) -> Result<BTreeMap<BTreeSet<Item>, StateInfo>, CombinedStatesError> {
        let mut ans = BTreeMap::from([(BTreeSet::from([Item::new(0, 0)]), StateInfo::default())]);
        let mut queue = ans.keys().cloned().collect::<VecDeque<_>>();
        let mut to_reprocess = BTreeSet::new();
        while let Some(core_items) = queue.pop_front() {
            let closure = self.closure_map(&core_items);
            let (action, goto) = self.process_state(&closure);
            for destination in goto.values() {
                let items = destination.key_set();
                if !ans.contains_key(&items) {
                    queue.push_back(items.clone());
                }
                ans.entry(items)
                    .or_insert(StateInfo::default())
                    .prev
                    .insert(core_items.clone());
            }
            let state_info = ans.get_mut(&core_items).unwrap();
            state_info.next = goto;

            let (action, conflicts) = action.one_to_one();
            let action = action.filter_map_keys(|k| k.item());
            if !conflicts.is_empty() {
                return Err(CombinedStatesError::Conflict {
                    core_items,
                    conflicts,
                });
            }

            let items = action.keys().copied().collect::<Vec<_>>();
            for i in 0..items.len() {
                let k1 = items[i];
                for j in (i + 1)..items.len() {
                    let k2 = items[j];
                    if action[&k1] != action[&k2] {
                        state_info.follow_exclusions.insert([k1, k2]);
                    }
                }
            }
            if !state_info.follow_exclusions.is_empty() {
                to_reprocess.insert(core_items);
            }
        }
        to_reprocess = to_reprocess
            .into_iter()
            .flat_map(|x| ans[&x].prev.iter())
            .cloned()
            .collect();
        while let Some(core_items) = to_reprocess.pop_first() {
            let closure = self.closure_map(&core_items);
            let (_, goto) = self.process_state(&closure);
            let mut new_set = BTreeSet::new();
            for value in goto.values() {
                let exclusions = &ans[&value.key_set()].follow_exclusions;
                for &[i1, i2] in exclusions {
                    for x in value[&i1].iter().filter_map(Follow::item) {
                        for y in value[&i2].iter().filter_map(Follow::item) {
                            match x.cmp(&y) {
                                Ordering::Less => {
                                    new_set.insert([x, y]);
                                }
                                Ordering::Equal => {
                                    return Err(CombinedStatesError::Conflict2 {
                                        core_items,
                                        item: x,
                                        target: value.key_set(),
                                    });
                                }
                                Ordering::Greater => {
                                    new_set.insert([y, x]);
                                }
                            }
                        }
                    }
                }
            }
            let data = ans.get_mut(&core_items).unwrap();
            if data.follow_exclusions.insert_many(&new_set) {
                to_reprocess.extend(data.prev.iter().cloned());
            }
        }
        Ok(ans)
    }
}

#[derive(Debug)]
pub enum CombinedStatesError {
    Conflict {
        core_items: BTreeSet<Item>,
        conflicts: BTreeMap<Follow, BTreeSet<Action>>,
    },
    Conflict2 {
        core_items: BTreeSet<Item>,
        item: Item,
        target: BTreeSet<Item>,
    },
}
